using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Mirror;
using Mirror.Authenticators;

public class AuthenthicationManager : MonoBehaviour
{

    public InputField UsernameInput;
    public InputField PasswordInput;
    public Button LoginButton;
    public GameObject NetworkManager;

    private BasicAuthenticator authenticator;
    private NetworkManager manager;

    // Start is called before the first frame update
    void Start()
    {
        manager = NetworkManager.GetComponent<NetworkManager>();
        authenticator = NetworkManager.GetComponent<BasicAuthenticator>();

        LoginButton.onClick.AddListener(() => {
            if (UsernameInput.text.Trim() == "host" && PasswordInput.text.Trim() == "host")
            {
                manager.StartServer();
            }
            else
            {
                authenticator.username = UsernameInput.text.Trim();
                authenticator.password = PasswordInput.text.Trim();

                manager.StartClient();
            }
        });
    }
}
